# Taller

### Prueba de Roles

### Indice

1. [Guest](#guest)
2. [Reporter](#reporter)
3. [Developer](#developer)
4. [Maintainer](#maintainer)


***

### ¿Que son los roles?

Los usuarios tienen diferentes habilidades dependiendo del nivel de acceso que tengan en un grupo o proyecto en particular. Si un usuario está tanto en el proyecto de un grupo como en el propio proyecto, se utiliza el nivel de permiso más alto.


***

### *Guest*
Este tipo de colaboradores solo pueden ver, descargar, dejar algun tipo de comentario y problema (issues) dentro del repositorio.
No puede realizar las siguientes actividades:
* No puede crear Labels.
* No se puede rear Milestone (sprints).
* No se puede agreagar /estimate y /spend dentro del Issue.
* No se puede crear nuevos boards.
* No se puede agregar list y Issues dentro de Board.
* No se puede asignar una persona al Issue.
* No se pueede hacer commits.
* No se puede editar el archivo readme.
* No se puede crear branchs.
* No se puede hacer push.
* No puede agregar etiquetas
* No puede asigar Issues.
* No puede ver estadisticas del pryecto
* No puede manejar licencias
* No se puede modificar el archivo README.MD
* Pueden dejar comentarios


***

### *Reporter*
Son los colaboradores de sólo lectura, ya que no pueden escribir en el repositorio.
Pueden realizar las siguientes actividades:
* Se puede asignar un colaborador al Issue.
* Se puede crear y agregar labels.
* Se puede agregar Milestones.
* Se puede agregar un peso.
* Se puede poner una fecha de entrega.
* No se puede hacer un push.
* Se puede crear ramas locales.
* Se puede mirar estadisticas del proyecto.
* No se puede crea nuevas ramas.
* No se puede crear un marge request.
* No se puede aceptar un marge request


***

#### *Developer*
Son los cotribuidores directos al proyecto, y tienen acceso a toda la parte de desallorrollo del repositorio.
Realizan las siguientes actividades:

* Se puede crear nuevos boards.
* Se puede crear push de las ramas.
* Se puede hacer commits.
* Se puede crear branch locales y remotas.
* No se puede hacer un commit desde la rama Developer a la rama master.
* Se puede crear un merge request.
* No puede agregar nuevos miembros al equipo.
* No se puede borrar Issues.
* Puede hacer push
* Se pueden crear branchs.
![image](/uploads/af2f6300858aaeec95d91557f48e1218/image.png)


***

#### *Maintainer*
Son desarrolladores con privilegios completos.
Pueden realizar las siguientes actividades:

* Se puede crear nuevos boards.
* Se puede crear push de las ramas.
* Se puede hacer commits.
* Se puede crear branch locales y remotas.
* Se puede crear un merge request.
* No puede agregar nuevos miembros al equipo.
* Puede borrar Issues.
* Puede hacer push.
* Se puede hacer un commit desde la rama Developer a la rama master.
![image](/uploads/a094b72de5e1f8d264395e4a609417a3/image.png)



## **([Regresar al INDICE](#indice))** 

